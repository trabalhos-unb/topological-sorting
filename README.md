# Topological sort in DAG graphs

The intuit of this project is to run two topological sort algorithms and compare its performance against different numbers of vertices.

# Running

You need gnuplot installed locally to run this project, if you don't have it type:

```
sudo apt install gnuplot-x11
```

Now go to the folder and type:

```
make run
```

or

```
make
make exec
```
